package com.tecsup.petclinic.service;

import java.util.List;

import com.tecsup.petclinic.domain.Vet;
import com.tecsup.petclinic.exception.VetNotFoundException;

public interface VetService {
	
	Vet create(Vet vet);
	Vet update(Vet vet);
	void delete(Long id) throws VetNotFoundException;
	Vet findById(long id) throws VetNotFoundException;
	List<Vet> findByFirstName(String firstName);
	Iterable<Vet> findAll();
	
}
