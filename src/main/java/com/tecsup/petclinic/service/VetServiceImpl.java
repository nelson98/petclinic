package com.tecsup.petclinic.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tecsup.petclinic.domain.Vet;
import com.tecsup.petclinic.domain.VetRepository;
import com.tecsup.petclinic.exception.VetNotFoundException;

@Service
public class VetServiceImpl implements VetService{
	
private static final Logger logger = LoggerFactory.getLogger(PetServiceImpl.class);
	
	@Autowired
	VetRepository vetRepository;
	
	@Override
	public Vet create(Vet vet) {
		return vetRepository.save(vet);
	}

	@Override
	public Vet update(Vet vet) {
		return vetRepository.save(vet);
	}

	@Override
	public void delete(Long id) throws VetNotFoundException {
		Vet vet = findById(id);
		vetRepository.delete(vet);
	}

	@Override
	public Vet findById(long id) throws VetNotFoundException {
		Optional<Vet> vet = vetRepository.findById(id);
		if ( !vet.isPresent())
			throw new VetNotFoundException("Record not found...!");
		return vet.get();
	}
	
	@Override
	public List<Vet> findByFirstName(String firstName) {
		List<Vet> vets = vetRepository.findByFirstName(firstName);
		vets.stream().forEach(vet -> logger.info("" + vet));
		return vets;
	}
	
	@Override
	public Iterable<Vet> findAll() {
		return vetRepository.findAll();
	}
	
}
