package com.tecsup.petclinic.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tecsup.petclinic.domain.Vet;
import com.tecsup.petclinic.exception.VetNotFoundException;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class VetServiceTest {

	private static final Logger logger = LoggerFactory.getLogger(VetServiceTest.class);

	@Autowired
	private VetService vetService;

	@Test
	public void testCreateVet() {
		String FIRST_NAME = "Carl";
		String LAST_NAME = "Johnson";

		Vet vet = new Vet(FIRST_NAME, LAST_NAME);
		vet = vetService.create(vet);
		logger.info("" + vet);

		assertThat(vet.getId()).isNotNull();
		assertEquals(FIRST_NAME, vet.getFirstName());
		assertEquals(LAST_NAME, vet.getLastName());
	}

	@Test
	public void testUpdateVet() {
		String FIRST_NAME = "Carl";
		String LAST_NAME = "Johnson";
		long create_id = -1;

		String UP_FIRST_NAME = "Alex";
		String UP_LAST_NAME = "Scott";
	
		Vet vet = new Vet(FIRST_NAME, LAST_NAME);

		// Create record
		logger.info(">" + vet);
		Vet readVet = vetService.create(vet);
		logger.info(">>" + readVet);

		create_id = readVet.getId();

		// Prepare data for update
		readVet.setFirstName(UP_FIRST_NAME);
		readVet.setLastName(UP_LAST_NAME);

		// Execute update
		Vet upgradeVet = vetService.update(readVet);
		logger.info(">>>>" + upgradeVet);

		assertThat(create_id).isNotNull();
		assertEquals(create_id, upgradeVet.getId());
		assertEquals(UP_FIRST_NAME, upgradeVet.getFirstName());
		assertEquals(UP_LAST_NAME, upgradeVet.getLastName());
	}

}
